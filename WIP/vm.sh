
# Crée la table de partitions
sudo parted /dev/sdb mklabel msdos
# crée une partition sur la totalité du disque
sudo parted --align optimal /dev/sdb mkpart primary ext4 0% 100%
# crée un système de fichier ext4 sur la partition nouvellement créée
sudo mkfs.ext4 /dev/sdb1
# désaloue les blocs réservés du système de fichier pour l'utliser à 100%
sudo tune2fs /dev/sdb1 -m 0
# création du point de montage des logs remote
sudo mkdir /repos

# mise à jour du fstab
sudo tee -a /etc/fstab << EOF 
# partition des mirroirs
/dev/sdb1 /repos ext4 rw,relatime,nosuid,nodev,noexec  1 2
EOF

# montage
sudo mount -a

# à voir pour la suite avec SELinux
# /sbin/restorecon -R -v /repos