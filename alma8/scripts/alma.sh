#!/bin/bash

BASE="/repos"

#yum install yum-utils nano createrepo -y
yum-config-manager --set-enabled plus
yum-config-manager --set-enabled powertools
#yum repolist all
#yum repolist -v

##### CREATION DES REPERTOIRES #####

BASE="/repos"

mkdir -p ${BASE}/repo.almalinux.org/almalinux/8/AppStream/x86_64/os/
mkdir -p ${BASE}/repo.almalinux.org/almalinux/8/BaseOS/x86_64/os/
mkdir -p ${BASE}/repo.almalinux.org/almalinux/8/extras/x86_64/os/
mkdir -p ${BASE}/repo.almalinux.org/almalinux/8/plus/x86_64/os/
mkdir -p ${BASE}/repo.almalinux.org/almalinux/8/PowerTools/x86_64/os/

##### TELECHARGEMENTS #####

curl -s https://repo.almalinux.org/almalinux/RPM-GPG-KEY-AlmaLinux     -o ${BASE}/repo.almalinux.org/almalinux/RPM-GPG-KEY-AlmaLinux
curl -s https://repo.almalinux.org/almalinux/RPM-GPG-KEY-AlmaLinux-8   -o ${BASE}/repo.almalinux.org/almalinux/RPM-GPG-KEY-AlmaLinux-8
curl -s https://repo.almalinux.org/almalinux/RPM-GPG-KEY-AlmaLinux-9   -o ${BASE}/repo.almalinux.org/almalinux/RPM-GPG-KEY-AlmaLinux-9

reposync --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'appstream'    -p ${BASE}/repo.almalinux.org/almalinux/8/AppStream/x86_64/os/
createrepo -g comps.xml ${BASE}/repo.almalinux.org/almalinux/8/AppStream/x86_64/os/

reposync --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'baseos'       -p ${BASE}/repo.almalinux.org/almalinux/8/BaseOS/x86_64/os/
createrepo -g comps.xml ${BASE}/repo.almalinux.org/almalinux/8/BaseOS/x86_64/os/

reposync --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'extras'       -p ${BASE}/repo.almalinux.org/almalinux/8/extras/x86_64/os/
createrepo -g comps.xml ${BASE}/repo.almalinux.org/almalinux/8/extras/x86_64/os/

reposync --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'plus'         -p ${BASE}/repo.almalinux.org/almalinux/8/plus/x86_64/os/
createrepo -g comps.xml ${BASE}/repo.almalinux.org/almalinux/8/plus/x86_64/os/

reposync --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'powertools'   -p ${BASE}/repo.almalinux.org/almalinux/8/PowerTools/x86_64/os/
createrepo -g comps.xml ${BASE}/repo.almalinux.org/almalinux/8/PowerTools/x86_64/os/
