#!/bin/bash

# docker-ce-stable
# 
# Repo-id            : docker-ce-stable
# Repo-name          : Docker CE Stable - x86_64
# Repo-revision      : 1685087483
# Repo-updated       : Fri May 26 07:51:23 2023
# Repo-pkgs          : 166
# Repo-available-pkgs: 166
# Repo-size          : 3.1 G
# Repo-baseurl       : https://download.docker.com/linux/centos/8/x86_64/stable
# Repo-expire        : 172800 second(s) (last: Sun Jun  4 20:50:14 2023)
# Repo-filename      : /etc/yum.repos.d/docker-ce.repo
# 

BASE="/repos"

echo "# DOCKER"

dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# [docker-ce-stable]
# name=Docker CE Stable - $basearch
# baseurl=https://download.docker.com/linux/centos/$releasever/$basearch/stable
# enabled=1
# gpgcheck=1
# gpgkey=https://download.docker.com/linux/centos/gpg
# 

mkdir -p ${BASE}/download.docker.com/linux/centos/8/x86_64/stable

curl -s https://download.docker.com/linux/centos/gpg -o ${BASE}/download.docker.com/linux/centos/gpg

# accepte l'import de la clé GPG
yum repolist -vy

reposync --download-metadata --downloadcomps --delete --newest-only --remote-time --norepopath --repo 'docker-ce-stable' -p ${BASE}/download.docker.com/linux/centos/8/x86_64/stable

createrepo ${BASE}/download.docker.com/linux/centos/8/x86_64/stable
