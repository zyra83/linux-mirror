#!/bin/bash
# exit when any command fails
set -e

BASE="/repos"

echo "# GRAYLOG 4.2"
# rpm -Uvh https://packages.graylog2.org/repo/packages/graylog-4.2-repository_latest.rpm

tee /etc/yum.repos.d/graylog42.repo > /dev/null <<EOT
[graylog-4.2]
name=graylog-4.2
baseurl=https://packages.graylog2.org/repo/el/stable/4.2/x86_64/
gpgcheck=1
repo_gpgcheck=0
gpgkey=https://packages.graylog2.org/repo/el/stable/GPG-KEY-graylog
EOT

mkdir -p ${BASE}/packages.graylog2.org/repo/packages/
mkdir -p ${BASE}/packages.graylog2.org/repo/el/stable/4.2/x86_64/

curl -s https://packages.graylog2.org/repo/el/stable/GPG-KEY-graylog -o ${BASE}/packages.graylog2.org/repo/el/stable/GPG-KEY-graylog
curl -s https://packages.graylog2.org/repo/packages/graylog-4.2-repository_latest.rpm -o ${BASE}/packages.graylog2.org/repo/packages/graylog-4.2-repository_latest.rpm

reposync --download-metadata --downloadcomps --delete --newest-only --remote-time --norepopath --repo 'graylog-4.2'       -p ${BASE}/packages.graylog2.org/repo/el/stable/4.2/x86_64/
createrepo ${BASE}/packages.graylog2.org/repo/el/stable/4.2/x86_64/
