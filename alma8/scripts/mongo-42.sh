#!/bin/bash

# Repo-id            : mongodb-org-4.2
# Repo-name          : MongoDB Repository
# Repo-revision      : 1666259438
# Repo-updated       : Thu Oct 20 09:53:31 2022
# Repo-pkgs          : 170
# Repo-available-pkgs: 170
# Repo-size          : 2.3 G
# Repo-baseurl       : https://repo.mongodb.org/yum/redhat/8/mongodb-org/4.2/x86_64/
# Repo-expire        : 172800 second(s) (last: Thu Oct 27 21:25:21 2022)
# Repo-filename      : /etc/yum.repos.d/mongodb-org.repo

BASE="/repos"

echo "# MONGO 4.2"

mkdir -p ${BASE}/repo.mongodb.org/yum/redhat/8/mongodb-org/4.2/x86_64/
mkdir -p ${BASE}/www.mongodb.org/static/pgp/
curl -s https://www.mongodb.org/static/pgp/server-4.2.asc -o ${BASE}/www.mongodb.org/static/pgp/server-4.2.asc

tee /etc/yum.repos.d/mongodb-org.repo > /dev/null <<EOT
[mongodb-org-4.2]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/8/mongodb-org/4.2/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-4.2.asc
EOT

reposync --download-metadata --downloadcomps --delete --newest-only --remote-time --norepopath --repo 'mongodb-org-4.2'       -p ${BASE}/repo.mongodb.org/yum/redhat/8/mongodb-org/4.2/x86_64/

createrepo ${BASE}/repo.mongodb.org/yum/redhat/8/mongodb-org/4.2/x86_64/
