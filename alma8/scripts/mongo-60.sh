#!/bin/bash

# Repo-id            : mongodb-org-6.0

BASE="/repos"

echo "# MONGO 6.0"

mkdir -p ${BASE}/repo.mongodb.org/yum/redhat/8/mongodb-org/6.0/x86_64/
mkdir -p ${BASE}/www.mongodb.org/static/pgp/
curl -s https://www.mongodb.org/static/pgp/server-6.0.asc -o ${BASE}/www.mongodb.org/static/pgp/server-6.0.asc

tee /etc/yum.repos.d/mongodb-org-6.0.repo > /dev/null <<EOT
[mongodb-org-6.0]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/8/mongodb-org/6.0/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-6.0.asc
EOT

reposync --download-metadata --downloadcomps --delete --newest-only --remote-time --norepopath --repo 'mongodb-org-6.0'       -p ${BASE}/repo.mongodb.org/yum/redhat/8/mongodb-org/6.0/x86_64/

createrepo ${BASE}/repo.mongodb.org/yum/redhat/8/mongodb-org/6.0/x86_64/
