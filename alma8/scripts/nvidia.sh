#!/bin/bash

BASE="/repos"

echo "# NVIDIA"
dnf config-manager --add-repo https://developer.download.nvidia.com/compute/cuda/repos/rhel8/x86_64/cuda-rhel8.repo
# https://developer.download.nvidia.com/compute/cuda/repos/rhel8/x86_64/D42D0685.pub

mkdir -p ${BASE}/developer.download.nvidia.com/compute/cuda/repos/rhel8/x86_64/
#curl -s http://download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG    -o ${BASE}/download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG   

# accepte les clés GPG avant le reposync
yum makecache -y

curl -s https://developer.download.nvidia.com/compute/cuda/repos/rhel8/x86_64/cuda-rhel8.repo -o ${BASE}/developer.download.nvidia.com/compute/cuda/repos/rhel8/x86_64/cuda-rhel8.repo

reposync  --download-metadata --newest-only --downloadcomps --delete --remote-time --norepopath --repo  'cuda-rhel8-x86_64' -p ${BASE}/developer.download.nvidia.com/compute/cuda/repos/rhel8/x86_64/

createrepo ${BASE}/developer.download.nvidia.com/compute/cuda/repos/rhel8/x86_64/
