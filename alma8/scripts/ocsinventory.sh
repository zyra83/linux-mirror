#!/bin/bash

# Repo-id            : ocsinventory
# Repo-name          : OCS Inventory NG repository for Enterprise Linux 8 - x86_64
# Repo-revision      : 1689169220
# Repo-updated       : Wed Jul 12 13:40:20 2023
# Repo-pkgs          : 51
# Repo-available-pkgs: 51
# Repo-size          : 43 M
# Repo-baseurl       : http://rpm.ocsinventory-ng.org/enterprise/8/x86_64/
# Repo-expire        : 172800 second(s) (last: Sun Jul 16 18:46:25 2023)
# Repo-filename      : /etc/yum.repos.d/ocsinventory.repo


BASE="/repos"

echo "# OCSINVENTORY pour RHEL 8"

dnf install https://rpm.ocsinventory-ng.org/ocsinventory-release-latest.el8.ocs.noarch.rpm -y

mkdir -p ${BASE}/rpm.ocsinventory-ng.org/enterprise/8/x86_64/

curl -s https://rpm.ocsinventory-ng.org/ocsinventory-release-latest.el8.ocs.noarch.rpm -o ${BASE}/rpm.ocsinventory-ng.org/ocsinventory-release-latest.el8.ocs.noarch.rpm

reposync -y --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'ocsinventory' -p ${BASE}/rpm.ocsinventory-ng.org/enterprise/8/x86_64/

createrepo ${BASE}/rpm.ocsinventory-ng.org/enterprise/8/x86_64/
