#!/bin/bash

# Repo-id            : remi-modular
# Repo-baseurl       : http://rpms.remirepo.net/enterprise/8/modular/x86_64/ (42 more)
# Repo-id            : remi-safe
# Repo-baseurl       : http://rpms.remirepo.net/enterprise/8/safe/x86_64/ (44 more)

BASE="/repos"

echo "# REMI PHP pour RHEL 8"
# Command to install the Remi repository configuration package:
yum install https://rpms.remirepo.net/enterprise/remi-release-8.rpm -y

mkdir -p ${BASE}/rpms.remirepo.net/enterprise/8/safe/x86_64/
mkdir -p ${BASE}/rpms.remirepo.net/enterprise/8/modular/x86_64/
mkdir -p ${BASE}/rpms.remirepo.net/enterprise/8/remi/x86_64/

curl -s https://rpms.remirepo.net/RPM-GPG-KEY-remi2023 -o ${BASE}/rpms.remirepo.net/RPM-GPG-KEY-remi2023
curl -s https://rpms.remirepo.net/RPM-GPG-KEY-remi2022 -o ${BASE}/rpms.remirepo.net/RPM-GPG-KEY-remi2022
curl -s https://rpms.remirepo.net/RPM-GPG-KEY-remi2021 -o ${BASE}/rpms.remirepo.net/RPM-GPG-KEY-remi2021
curl -s https://rpms.remirepo.net/RPM-GPG-KEY-remi2020 -o ${BASE}/rpms.remirepo.net/RPM-GPG-KEY-remi2020
curl -s https://rpms.remirepo.net/RPM-GPG-KEY-remi2019 -o ${BASE}/rpms.remirepo.net/RPM-GPG-KEY-remi2019
curl -s https://rpms.remirepo.net/RPM-GPG-KEY-remi2018 -o ${BASE}/rpms.remirepo.net/RPM-GPG-KEY-remi2018
curl -s https://rpms.remirepo.net/RPM-GPG-KEY-remi2017 -o ${BASE}/rpms.remirepo.net/RPM-GPG-KEY-remi2017
curl -s https://rpms.remirepo.net/RPM-GPG-KEY-remi     -o ${BASE}/rpms.remirepo.net/RPM-GPG-KEY-remi

reposync -y --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'remi-safe' -p ${BASE}/rpms.remirepo.net/enterprise/8/safe/x86_64/
reposync -y --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'remi-modular' -p ${BASE}/rpms.remirepo.net/enterprise/8/modular/x86_64/
reposync -y --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'remi' -p ${BASE}/rpms.remirepo.net/enterprise/8/remi/x86_64/

createrepo ${BASE}/rpms.remirepo.net/enterprise/8/safe/x86_64/
createrepo ${BASE}/rpms.remirepo.net/enterprise/8/modular/x86_64/
createrepo ${BASE}/rpms.remirepo.net/enterprise/8/remi/x86_64/
