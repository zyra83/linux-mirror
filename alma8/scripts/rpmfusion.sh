#!/bin/bash

# Repo-id            : rpmfusion-free-updates
# Repo-name          : RPM Fusion for EL 8 - Free - Updates
# Repo-revision      : 1667206538
# Repo-tags          : binary-x86_64
# Repo-updated       : Mon Oct 31 08:55:40 2022
# Repo-pkgs          : 222
# Repo-available-pkgs: 222
# Repo-size          : 296 M
# Repo-mirrors       : http://mirrors.rpmfusion.org/mirrorlist?repo=free-el-updates-released-8&arch=x86_64
# Repo-baseurl       : http://download1.rpmfusion.org/free/el/updates/8/x86_64/ (12 more)
# Repo-expire        : 172800 second(s) (last: Tue Nov  1 21:07:05 2022)
# Repo-filename      : /etc/yum.repos.d/rpmfusion-free-updates.repo

# Repo-id            : rpmfusion-nonfree-updates
# Repo-name          : RPM Fusion for EL 8 - Nonfree - Updates
# Repo-revision      : 1667207055
# Repo-tags          : binary-x86_64
# Repo-updated       : Mon Oct 31 09:04:17 2022
# Repo-pkgs          : 95
# Repo-available-pkgs: 95
# Repo-size          : 1.1 G
# Repo-mirrors       : http://mirrors.rpmfusion.org/mirrorlist?repo=nonfree-el-updates-released-8&arch=x86_64
# Repo-baseurl       : http://fr2.rpmfind.net/linux/rpmfusion/nonfree/el/updates/8/x86_64/ (12 more)
# Repo-expire        : 172800 second(s) (last: Tue Nov  1 21:07:06 2022)
# Repo-filename      : /etc/yum.repos.d/rpmfusion-nonfree-updates.repo

dnf install https://download1.rpmfusion.org/free/el/rpmfusion-free-release-8.noarch.rpm https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-8.noarch.rpm -y

BASE="/repos"

echo "# RPMFUSION"
                                                                                                
curl -s https://download1.rpmfusion.org/free/el/RPM-GPG-KEY-rpmfusion-free-el-8       -o ${BASE}/download1.rpmfusion.org/free/el/RPM-GPG-KEY-rpmfusion-free-el-8
curl -s https://download1.rpmfusion.org/nonfree/el/RPM-GPG-KEY-rpmfusion-nonfree-el-8 -o ${BASE}/download1.rpmfusion.org/nonfree/el/RPM-GPG-KEY-rpmfusion-nonfree-el-8

mkdir -p ${BASE}/download1.rpmfusion.org/free/el/updates/8/x86_64/
mkdir -p ${BASE}/download1.rpmfusion.org/nonfree/el/updates/8/x86_64/

reposync  --download-metadata --downloadcomps --delete --remote-time --norepopath --repo  'rpmfusion-free-updates'    -p ${BASE}/download1.rpmfusion.org/free/el/updates/8/x86_64/
reposync  --download-metadata --downloadcomps --delete --remote-time --norepopath --repo  'rpmfusion-nonfree-updates' -p ${BASE}/download1.rpmfusion.org/nonfree/el/updates/8/x86_64/
 
createrepo ${BASE}/download1.rpmfusion.org/free/el/updates/8/x86_64/
createrepo ${BASE}/download1.rpmfusion.org/nonfree/el/updates/8/x86_64/
