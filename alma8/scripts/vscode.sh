#!/bin/bash

# Repo-id            : code
# Repo-name          : Visual Studio Code
# Repo-revision      : 1689578809
# Repo-updated       : Mon Jul 17 07:26:45 2023
# Repo-pkgs          : 3553
# Repo-available-pkgs: 3553
# Repo-size          : 361 G
# Repo-baseurl       : https://packages.microsoft.com/yumrepos/vscode
# Repo-expire        : 172800 second(s) (last: Mon Jul 17 16:54:47 2023)
# Repo-filename      : /etc/yum.repos.d/vscode.repo

BASE="/repos"

echo "# VSCODE"

rpm --import https://packages.microsoft.com/keys/microsoft.asc

tee /etc/yum.repos.d/vscode.repo << EOF 
[code]
name=Visual Studio Code
baseurl=https://packages.microsoft.com/yumrepos/vscode
enabled=1
gpgcheck=1
gpgkey=https://packages.microsoft.com/keys/microsoft.asc
EOF

mkdir -p ${BASE}/packages.microsoft.com/keys/
mkdir -p ${BASE}/packages.microsoft.com/yumrepos/vscode
curl -s https://packages.microsoft.com/keys/microsoft.asc    -o ${BASE}/packages.microsoft.com/keys/microsoft.asc

# accepte les clés GPG avant le reposync
yum makecache -y

reposync  --arch x86_64 --download-metadata --newest-only --downloadcomps --delete --remote-time --norepopath --repo  'code' -p ${BASE}/packages.microsoft.com/yumrepos/vscode

# createrepo ne semble pas nécessaire pour les reposync de RHEL 8 mais je ne prends que latest
createrepo ${BASE}/packages.microsoft.com/yumrepos/vscode
