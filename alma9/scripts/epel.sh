#!/bin/bash

BASE="/repos"

echo "# EPEL ALMA 9"

yum install epel-release -y

# epel
mkdir -p ${BASE}/download.fedoraproject.org/pub/epel/9/Everything/x86_64/
reposync --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'epel' -p ${BASE}/download.fedoraproject.org/pub/epel/9/Everything/x86_64/
createrepo -g comps.xml ${BASE}/download.fedoraproject.org/pub/epel/9/Everything/x86_64/
