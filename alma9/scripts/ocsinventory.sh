#!/bin/bash

# Repo-id            : ocsinventory
# Repo-name          : OCS Inventory NG repository for Enterprise Linux 9 - x86_64
# Repo-baseurl       : http://rpm.ocsinventory-ng.org/enterprise/9/x86_64/


BASE="/repos"

echo "# OCSINVENTORY pour RHEL 9"

dnf install https://rpm.ocsinventory-ng.org/ocsinventory-release-latest.el9.ocs.noarch.rpm -y

mkdir -p ${BASE}/rpm.ocsinventory-ng.org/enterprise/9/x86_64/

curl -s https://rpm.ocsinventory-ng.org/ocsinventory-release-latest.el9.ocs.noarch.rpm -o ${BASE}/rpm.ocsinventory-ng.org/ocsinventory-release-latest.el9.ocs.noarch.rpm

reposync -y --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'ocsinventory' -p ${BASE}/rpm.ocsinventory-ng.org/enterprise/9/x86_64/

createrepo ${BASE}/rpm.ocsinventory-ng.org/enterprise/9/x86_64/
