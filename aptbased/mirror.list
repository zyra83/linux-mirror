############ config ##################
#
set base_path    /var/spool/apt-mirror
#
set mirror_path  /var/spool/apt-mirror/mirror/
set skel_path    $base_path/skel
set var_path     $base_path/var
set cleanscript $var_path/clean.sh
# set defaultarch  <running host architecture>
set postmirror_script $var_path/postmirror.sh
set run_postmirror 1
set nthreads     20
set _tilde 0
# limite le débit de téléchargement à 0.25 MBps par thread
#set limit_rate 0.25m
#set use_proxy         off
#set http_proxy        127.0.0.1:3128
#set proxy_user        user
#set proxy_password    passwor

#
############# end config ##############

#-----------------------------------#
# OFFICIAL DEBIAN REPOS BOOKWORM (12) sortie le 10 juin 2023
#-----------------------------------#

# deb http://deb.debian.org/debian bookworm main contrib non-free
# deb http://security.debian.org/debian-security bookworm-security main contrib non-free
# deb http://deb.debian.org/debian bookworm-updates main contrib non-free
# deb http://deb.debian.org/debian bookworm-backports main contrib non-free

#-----------------------------------#
# OFFICIAL DEBIAN REPOS BULSEYE (11)
#-----------------------------------#

deb http://deb.debian.org/debian bullseye main contrib non-free
deb http://security.debian.org/debian-security bullseye-security main contrib non-free
deb http://deb.debian.org/debian bullseye-updates main contrib non-free
deb http://deb.debian.org/debian bullseye-backports main contrib non-free

#-----------------------------------#
# OFFICIAL DEBIAN REPOS BUSTER (10) EOL au 30/06/2024
#-----------------------------------#

deb http://deb.debian.org/debian buster main contrib non-free
deb http://security.debian.org/debian-security buster/updates main contrib non-free
deb http://deb.debian.org/debian buster-updates main contrib non-free
deb http://deb.debian.org/debian buster-backports main

#-----------------------------------#
# OFFICIAL DEBIAN REPOS STRETCH (9) EOL au 30/06/2022
#-----------------------------------#

deb http://archive.debian.org/debian stretch main contrib non-free
deb https://archive.debian.org/debian-security/ stretch/updates main contrib non-free
deb http://archive.debian.org/debian stretch-backports main contrib non-free

#-----------------------------------#
# OFFICIAL DEBIAN REPOS JESSIE (8) EOL au 30/06/2020
#-----------------------------------#

deb http://archive.debian.org/debian/ jessie main contrib non-free
deb https://archive.debian.org/debian-security/ jessie/updates main contrib non-free
deb http://archive.debian.org/debian/ jessie-backports main contrib non-free

#------------------------------------#
# OFFICIAL UBUNTU REPOS FOCAL (20.04)
#------------------------------------#

deb http://archive.ubuntu.com/ubuntu focal main restricted universe multiverse
deb http://archive.ubuntu.com/ubuntu focal-updates main restricted universe multiverse
deb http://archive.ubuntu.com/ubuntu focal-backports main restricted universe multiverse
deb http://archive.ubuntu.com/ubuntu focal-security main restricted universe multiverse

# deb http://archive.canonical.com/ubuntu focal partner

#------------------------------------#
# OFFICIAL UBUNTU REPOS JAMMY (22.04)
#------------------------------------#

deb http://archive.ubuntu.com/ubuntu jammy main restricted universe multiverse
deb http://archive.ubuntu.com/ubuntu jammy-security main restricted universe multiverse
deb http://archive.ubuntu.com/ubuntu jammy-updates main restricted universe multiverse
# deb http://archive.ubuntu.com/ubuntu jammy-proposed main restricted universe multiverse
deb http://archive.ubuntu.com/ubuntu jammy-backports main restricted universe multiverse

#------------------------------------|
# RASPBIAN BUSTER 
#------------------------------------#

# deb http://raspbian.raspberrypi.org/raspbian/ buster main contrib non-free rpi

#------------------------------------------------------------------------------#
#                      UNOFFICIAL  REPOS
#------------------------------------------------------------------------------#

###### 3rd Party Binary Repos
###Docker CE
deb https://download.docker.com/linux/debian bullseye stable
deb https://download.docker.com/linux/debian buster stable
deb https://download.docker.com/linux/debian stretch stable
deb https://download.docker.com/linux/ubuntu focal stable
deb https://download.docker.com/linux/ubuntu jammy stable

### Nvidia-Docker
deb https://nvidia.github.io/libnvidia-container/stable/debian9/amd64  /
deb https://nvidia.github.io/nvidia-container-runtime/stable/debian9/amd64 /
deb https://nvidia.github.io/nvidia-docker/debian9/amd64 /
deb https://nvidia.github.io/libnvidia-container/stable/debian10/amd64  /
deb https://nvidia.github.io/nvidia-container-runtime/stable/debian10/amd64 /
deb https://nvidia.github.io/nvidia-docker/debian10/amd64 /
deb https://nvidia.github.io/libnvidia-container/stable/ubuntu20.04/amd64  /
deb https://nvidia.github.io/nvidia-container-runtime/stable/ubuntu20.04/amd64 /
deb https://nvidia.github.io/nvidia-docker/ubuntu20.04/amd64 /

### ZABBIX
deb https://repo.zabbix.com/zabbix/6.0/debian bullseye main
deb [arch=amd64] https://repo.zabbix.com/zabbix-agent2-plugins/1/debian bullseye main
deb https://repo.zabbix.com/zabbix/6.0/debian bullseye main
deb [arch=amd64] https://repo.zabbix.com/zabbix-agent2-plugins/1/debian buster main

### MOZILLA PPA
deb [arch=amd64] https://ppa.launchpadcontent.net/mozillateam/ppa/ubuntu/ jammy main

### OCSINVENTORY
deb http://deb.ocsinventory-ng.org/debian/ bullseye main
deb http://deb.ocsinventory-ng.org/debian/ buster main
deb http://deb.ocsinventory-ng.org/debian/ stretch main
deb http://deb.ocsinventory-ng.org/ubuntu/ focal main
deb http://deb.ocsinventory-ng.org/ubuntu/ jammy main

### vscode
deb [arch=amd64] https://packages.microsoft.com/repos/code stable main

### Sury/PHP
deb https://packages.sury.org/php/ buster main
deb https://packages.sury.org/php/ bullseye main

#------------------------------------------------------------------------------#
#                      CLEANUP
#------------------------------------------------------------------------------#

clean http://ftp.debian.org/debian
clean http://deb.debian.org/debian/
clean http://deb.debian.org/debian-security
clean https://archive.debian.org/debian-security/
clean http://archive.debian.org/debian/
clean https://download.docker.com/linux/debian
clean https://download.docker.com/linux/ubuntu
clean http://archive.ubuntu.com/ubuntu
clean http://raspbian.raspberrypi.org/raspbian/
clean http://security.ubuntu.com/ubuntu/
clean http://security.debian.org/debian-security
clean https://repo.zabbix.com/zabbix/6.0/debian
clean https://repo.zabbix.com/zabbix-agent2-plugins/1/debian
clean https://ppa.launchpadcontent.net/mozillateam/ppa/ubuntu/
clean http://deb.ocsinventory-ng.org/debian/
clean http://deb.ocsinventory-ng.org/ubuntu/
clean https://packages.microsoft.com/repos/code
clean https://packages.sury.org/php/