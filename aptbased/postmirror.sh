#!/bin/bash
echo "Lancement de clean.sh"
/var/spool/apt-mirror/var/clean.sh
echo "Fin de clean.sh"

echo "début des téléchargements"
mkdir -p /var/spool/apt-mirror/mirror/nvidia.github.io/nvidia-docker/
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey                           > /var/spool/apt-mirror/mirror/nvidia.github.io/nvidia-docker/gpgkey

mkdir -p repo.zabbix.com/zabbix/6.0/debian/dists/bullseye
curl -s -L https://repo.zabbix.com/zabbix/6.0/debian/dists/bullseye/Release        > /var/spool/apt-mirror/mirror/repo.zabbix.com/zabbix/6.0/debian/dists/bullseye/Release
curl -s -L https://repo.zabbix.com/zabbix/6.0/debian/dists/bullseye/Release.gpg    > /var/spool/apt-mirror/mirror/repo.zabbix.com/zabbix/6.0/debian/dists/bullseye/Release.gpg

mkdir -p repo.zabbix.com/zabbix/6.0/debian/dists/buster
curl -s -L https://repo.zabbix.com/zabbix/6.0/debian/dists/buster/Release          > /var/spool/apt-mirror/mirror/repo.zabbix.com/zabbix/6.0/debian/dists/buster/Release
curl -s -L https://repo.zabbix.com/zabbix/6.0/debian/dists/buster/Release.gpg      > /var/spool/apt-mirror/mirror/repo.zabbix.com/zabbix/6.0/debian/dists/buster/Release.gpg

mkdir -p packages.microsoft.com/keys/
curl -s -L https://packages.microsoft.com/keys/microsoft.asc                       > /var/spool/apt-mirror/mirror/packages.microsoft.com/keys/microsoft.asc

echo "fin des téléchargements"
