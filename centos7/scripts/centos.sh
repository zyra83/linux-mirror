#!/bin/bash

BASE="/repos"

# base/7/x86_64
mkdir -p ${BASE}/mirror.centos.org/centos/7/os/x86_64/
# extras/7/x86_64
mkdir -p ${BASE}/mirror.centos.org/centos/7/extras/x86_64/
# updates/7/x86_64
mkdir -p ${BASE}/mirror.centos.org/centos/7/updates/x86_64/
# centosplus/7/x86_64
mkdir -p ${BASE}/mirror.centos.org/centos/7/centosplus/x86_64/

##### TELECHARGEMENTS #####

curl -s http://mirror.centos.org/centos/RPM-GPG-KEY-beta               -o ${BASE}/mirror.centos.org/centos/RPM-GPG-KEY-beta             
curl -s http://mirror.centos.org/centos/RPM-GPG-KEY-CentOS-3           -o ${BASE}/mirror.centos.org/centos/RPM-GPG-KEY-CentOS-3         
curl -s http://mirror.centos.org/centos/RPM-GPG-KEY-centos4            -o ${BASE}/mirror.centos.org/centos/RPM-GPG-KEY-centos4          
curl -s http://mirror.centos.org/centos/RPM-GPG-KEY-CentOS-4           -o ${BASE}/mirror.centos.org/centos/RPM-GPG-KEY-CentOS-4         
curl -s http://mirror.centos.org/centos/RPM-GPG-KEY-CentOS-5           -o ${BASE}/mirror.centos.org/centos/RPM-GPG-KEY-CentOS-5         
curl -s http://mirror.centos.org/centos/RPM-GPG-KEY-CentOS-6           -o ${BASE}/mirror.centos.org/centos/RPM-GPG-KEY-CentOS-6         
curl -s http://mirror.centos.org/centos/RPM-GPG-KEY-CentOS-7           -o ${BASE}/mirror.centos.org/centos/RPM-GPG-KEY-CentOS-7         
curl -s http://mirror.centos.org/centos/RPM-GPG-KEY-CentOS-Debug-6     -o ${BASE}/mirror.centos.org/centos/RPM-GPG-KEY-CentOS-Debug-6   
curl -s http://mirror.centos.org/centos/RPM-GPG-KEY-CentOS-Debug-7     -o ${BASE}/mirror.centos.org/centos/RPM-GPG-KEY-CentOS-Debug-7   
curl -s http://mirror.centos.org/centos/RPM-GPG-KEY-CentOS-Official    -o ${BASE}/mirror.centos.org/centos/RPM-GPG-KEY-CentOS-Official  
curl -s http://mirror.centos.org/centos/RPM-GPG-KEY-CentOS-Security-6  -o ${BASE}/mirror.centos.org/centos/RPM-GPG-KEY-CentOS-Security-6
curl -s http://mirror.centos.org/centos/RPM-GPG-KEY-CentOS-Testing-6   -o ${BASE}/mirror.centos.org/centos/RPM-GPG-KEY-CentOS-Testing-6 
curl -s http://mirror.centos.org/centos/RPM-GPG-KEY-CentOS-Testing-7   -o ${BASE}/mirror.centos.org/centos/RPM-GPG-KEY-CentOS-Testing-7 

echo
reposync --download-metadata -n -l -m -d --norepopath -r 'base' -p ${BASE}/mirror.centos.org/centos/7/os/x86_64/
createrepo -g comps.xml ${BASE}/mirror.centos.org/centos/7/os/x86_64/

reposync --download-metadata -n -l -m -d --norepopath -r 'extras' -p ${BASE}/mirror.centos.org/centos/7/extras/x86_64/
createrepo ${BASE}/mirror.centos.org/centos/7/extras/x86_64/

reposync --download-metadata -n -l -m -d --norepopath -r 'updates' -p ${BASE}/mirror.centos.org/centos/7/updates/x86_64/
createrepo ${BASE}/mirror.centos.org/centos/7/updates/x86_64/

reposync --download-metadata -n -l -m -d --norepopath -r 'centosplus' -p ${BASE}/mirror.centos.org/centos/7/centosplus/x86_64/
createrepo ${BASE}/mirror.centos.org/centos/7/centosplus/x86_64/
