#!/bin/bash

BASE="/repos"

echo "# GRAYLOG"
# https://packages.graylog2.org/repo/packages/graylog-4.2-repository_latest.rpm
rpm -Uvh https://packages.graylog2.org/repo/packages/graylog-4.2-repository_latest.rpm

mkdir -p ${BASE}/packages.graylog2.org/repo/packages/
mkdir -p ${BASE}/packages.graylog2.org/repo/el/stable/4.2/x86_64/
curl -s https://packages.graylog2.org/repo/packages/graylog-4.2-repository_latest.rpm -o ${BASE}/packages.graylog2.org/repo/packages/graylog-4.2-repository_latest.rpm

reposync --download-metadata -n -l -m -d --norepopath -r 'graylog' -p ${BASE}/packages.graylog2.org/repo/el/stable/4.2/x86_64/
createrepo ${BASE}/packages.graylog2.org/repo/el/stable/4.2/x86_64/
