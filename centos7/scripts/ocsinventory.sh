#!/bin/bash

# Repo-id      : ocsinventory/x86_64
# Repo-name    : OCS Inventory NG repository for Enterprise Linux 7 - x86_64
# Repo-revision: 1689169176
# Repo-updated : Wed Jul 12 13:39:36 2023
# Repo-pkgs    : 84
# Repo-size    : 51 M
# Repo-baseurl : http://rpm.ocsinventory-ng.org/enterprise/7/x86_64/
# Repo-expire  : 21600 second(s) (last: Sun Jul 16 18:58:37 2023)
#   Filter     : read-only:present
# Repo-filename: /etc/yum.repos.d/ocsinventory.repo

BASE="/repos"

echo "# OCSINVENTORY pour RHEL 7"

yum install https://rpm.ocsinventory-ng.org/ocsinventory-release-latest.el7.ocs.noarch.rpm -y

mkdir -p ${BASE}/rpm.ocsinventory-ng.org/enterprise/8/x86_64/

curl -s https://rpm.ocsinventory-ng.org/ocsinventory-release-latest.el7.ocs.noarch.rpm -o ${BASE}/rpm.ocsinventory-ng.org/ocsinventory-release-latest.el7.ocs.noarch.rpm

reposync --download-metadata -l -m -d --norepopath -r 'ocsinventory' -p ${BASE}/rpm.ocsinventory-ng.org/enterprise/7/x86_64/

createrepo ${BASE}/rpm.ocsinventory-ng.org/enterprise/7/x86_64/
