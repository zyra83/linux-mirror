#!/bin/bash

BASE="/repos"

echo "# REMI PHP"
# Command to install the Remi repository configuration package:
yum install https://rpms.remirepo.net/enterprise/remi-release-7.rpm -y

mkdir -p ${BASE}/rpms.remirepo.net/enterprise/7/safe/x86_64/
mkdir -p ${BASE}/rpms.remirepo.net/enterprise/7/modular/x86_64/
mkdir -p ${BASE}/rpms.remirepo.net/enterprise/7/php56/x86_64/
mkdir -p ${BASE}/rpms.remirepo.net/enterprise/7/php74/x86_64/
mkdir -p ${BASE}/rpms.remirepo.net/enterprise/7/php80/x86_64/
mkdir -p ${BASE}/rpms.remirepo.net/enterprise/7/php81/x86_64/

curl -s https://rpms.remirepo.net/RPM-GPG-KEY-remi2023 -o ${BASE}/rpms.remirepo.net/RPM-GPG-KEY-remi2023
curl -s https://rpms.remirepo.net/RPM-GPG-KEY-remi2022 -o ${BASE}/rpms.remirepo.net/RPM-GPG-KEY-remi2022
curl -s https://rpms.remirepo.net/RPM-GPG-KEY-remi2021 -o ${BASE}/rpms.remirepo.net/RPM-GPG-KEY-remi2021
curl -s https://rpms.remirepo.net/RPM-GPG-KEY-remi2020 -o ${BASE}/rpms.remirepo.net/RPM-GPG-KEY-remi2020
curl -s https://rpms.remirepo.net/RPM-GPG-KEY-remi2019 -o ${BASE}/rpms.remirepo.net/RPM-GPG-KEY-remi2019
curl -s https://rpms.remirepo.net/RPM-GPG-KEY-remi2018 -o ${BASE}/rpms.remirepo.net/RPM-GPG-KEY-remi2018
curl -s https://rpms.remirepo.net/RPM-GPG-KEY-remi2017 -o ${BASE}/rpms.remirepo.net/RPM-GPG-KEY-remi2017
curl -s https://rpms.remirepo.net/RPM-GPG-KEY-remi     -o ${BASE}/rpms.remirepo.net/RPM-GPG-KEY-remi

reposync --download-metadata -n -l -m -d --norepopath -r 'remi-safe' -p ${BASE}/rpms.remirepo.net/enterprise/7/safe/x86_64/
reposync --download-metadata -n -l -m -d --norepopath -r 'remi-modular' -p ${BASE}/rpms.remirepo.net/enterprise/7/modular/x86_64/
reposync --download-metadata -n -l -m -d --norepopath -r 'remi-php56' -p ${BASE}/rpms.remirepo.net/enterprise/7/php56/x86_64/
reposync --download-metadata -n -l -m -d --norepopath -r 'remi-php74' -p ${BASE}/rpms.remirepo.net/enterprise/7/php74/x86_64/
reposync --download-metadata -n -l -m -d --norepopath -r 'remi-php80' -p ${BASE}/rpms.remirepo.net/enterprise/7/php80/x86_64/
reposync --download-metadata -n -l -m -d --norepopath -r 'remi-php81' -p ${BASE}/rpms.remirepo.net/enterprise/7/php81/x86_64/

createrepo ${BASE}/rpms.remirepo.net/enterprise/7/safe/x86_64/
createrepo ${BASE}/rpms.remirepo.net/enterprise/7/modular/x86_64/
createrepo ${BASE}/rpms.remirepo.net/enterprise/7/php56/x86_64/
createrepo ${BASE}/rpms.remirepo.net/enterprise/7/php74/x86_64/
createrepo ${BASE}/rpms.remirepo.net/enterprise/7/php80/x86_64/
createrepo ${BASE}/rpms.remirepo.net/enterprise/7/php81/x86_64/
