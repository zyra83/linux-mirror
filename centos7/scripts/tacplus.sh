#!/bin/bash

echo "# TACACSPLUS"
# TACACS+ https://networklessons.com/uncategorized/how-to-install-tacacs-on-linux-centos
# "F:\Workspace\Sysadmin\graylog\README.md"

mkdir -p ${BASE}/li.nux.ro/download/nux/misc/el7/x86_64/
curl -s http://li.nux.ro/download/nux/RPM-GPG-KEY-nux.ro -o ${BASE}/li.nux.ro/download/nux/RPM-GPG-KEY-nux.ro

tee /etc/yum.repos.d/nux-misc.repo > /dev/null <<EOT
[nux-misc]
name=Nux Misc
baseurl=http://li.nux.ro/download/nux/misc/el7/x86_64/
enabled=1
gpgcheck=1
gpgkey=http://li.nux.ro/download/nux/RPM-GPG-KEY-nux.ro
EOT
reposync --download-metadata -n -l -m -d --norepopath -r 'nux-misc' -p ${BASE}/li.nux.ro/download/nux/misc/el7/x86_64/
createrepo ${BASE}/li.nux.ro/download/nux/misc/el7/x86_64/
