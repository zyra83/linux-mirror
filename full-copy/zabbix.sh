#!/bin/bash

# ces commandes créent le dossier repo.zabbix.com
wget -r -np -R "index.html*" http://repo.zabbix.com/non-supported/
wget -r -np -R "index.html*" http://repo.zabbix.com/zabbix/6.0/
wget -r -np -R "index.html*" http://repo.zabbix.com/zabbix-agent2-plugins/

cd repo.zabbix.com
wget http://repo.zabbix.com/RPM-GPG-KEY-ZABBIX
wget http://repo.zabbix.com/RPM-GPG-KEY-ZABBIX-08EFA7DD
wget http://repo.zabbix.com/RPM-GPG-KEY-ZABBIX-79EA5ED4
wget http://repo.zabbix.com/RPM-GPG-KEY-ZABBIX-A14FE591
wget http://repo.zabbix.com/RPM-GPG-KEY-ZABBIX-A14FE591-EL5
wget http://repo.zabbix.com/zabbix-official-repo.key
